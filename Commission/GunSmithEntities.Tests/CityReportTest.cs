using CommunEntities;
// <copyright file="CityReportTest.cs">Copyright �  2015</copyright>

using System;
using GunSmithEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GunSmithEntities
{
    [TestClass]
    [PexClass(typeof(CityReport))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class CityReportTest
    {
        [PexMethod]
        public CityReport Constructor(
            MonthReport monthReport,
            float totalAmount,
            City city
        )
        {
            CityReport target = new CityReport(monthReport, totalAmount, city);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode CityReportTest.Constructor(MonthReport, Single, City)
        }
    }
}
