using CommunEntities;
// <copyright file="MonthReportTest.cs">Copyright �  2015</copyright>

using System;
using GunSmithEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GunSmithEntities
{
    [TestClass]
    [PexClass(typeof(MonthReport))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class MonthReportTest
    {
        [PexMethod]
        public MonthReport Constructor01(
            SalesPerson seller,
            DateTime month,
            DateTime? endMonth,
            float totalIncome,
            float commission
        )
        {
            MonthReport target = new MonthReport(seller, month, endMonth, totalIncome, commission);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode MonthReportTest.Constructor01(SalesPerson, DateTime, Nullable`1<DateTime>, Single, Single)
        }
        [PexMethod]
        public MonthReport Constructor(SalesPerson seller, DateTime month)
        {
            MonthReport target = new MonthReport(seller, month);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode MonthReportTest.Constructor(SalesPerson, DateTime)
        }
    }
}
