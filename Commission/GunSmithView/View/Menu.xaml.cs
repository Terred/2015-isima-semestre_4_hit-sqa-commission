﻿using GunSmithBusinessLayer;
using GunSmithView.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GunSmithView
{
    /// <summary>
    /// Logique d'interaction pour Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
            this.DataContext = new MenuViewModel();
            ((MenuViewModel)this.DataContext).ManageProductsAction += () => { InventoryGestion win = new InventoryGestion(); win.Show(); };
            ((MenuViewModel)this.DataContext).ManageSalesPersonsAction += () => { SalespersonsGestion win = new SalespersonsGestion(); win.Show(); };
            ((MenuViewModel)this.DataContext).ShowSellsAction += () => { SalesHistory win = new SalesHistory(); win.Show(); };
        }

        private void Window_Menu_Closed(object sender, EventArgs e)
        {
            Authentification.unAuthentifyGunSmith();
            Application.Current.Shutdown(0);
        }

        private void Window_Focus(object sender, EventArgs e)
        {
            ((MenuViewModel)this.DataContext).reload();
        }
    }
}
