﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithView.ViewModel
{
    public class InventoryGestionProductViewModel : ViewModelBase
    {
        public Product Product { get; private set; }

        public InventoryGestionProductViewModel(Product product)
        {
            if (product != null)
                Product = product;
            else
                Product = new Product();
        }

        public String Name
        {
            get { return Product.Name; }
            set
            {
                if (value == Product.Name) return;
                Product.Name = value;
                base.OnPropertyChanged("Name");
            }
        }

        public bool SaleMandatory
        {
            get { return Product.SaleMandatory; }
            set
            {
                if (value == Product.SaleMandatory) return;
                Product.SaleMandatory = value;
                base.OnPropertyChanged("SaleMandatory");
            }
        }

        public float Price
        {
            get { return Product.Price; }
            set
            {
                if (value == Product.Price) return;
                Product.Price = value;
                base.OnPropertyChanged("Price");
            }
        }

        public int Amount
        {
            get { return Product.Amount; }
            set
            {
                if (value == Product.Amount) return;
                Product.Amount = value;
                base.OnPropertyChanged("Amount");
            }
        }
    }
}
