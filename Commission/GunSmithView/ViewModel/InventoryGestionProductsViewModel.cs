﻿using CommunEntities;
using GunSmithBusinessLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithView.ViewModel
{
    public class InventoryGestionProductsViewModel : ViewModelBase
    {
        private ObservableCollection<InventoryGestionProductViewModel> mProducts;
        private bool isAddProduct = false;

        public ObservableCollection<InventoryGestionProductViewModel> Products
        {
            get { return mProducts; }
            private set
            {
                mProducts = value;
                OnPropertyChanged("Products");
            }
        }

        private InventoryGestionProductViewModel mSelectedItem;
        public InventoryGestionProductViewModel SelectedItem
        {
            get { return mSelectedItem; }
            set
            {
                mSelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public InventoryGestionProductsViewModel() 
        {
            UpdateList();
            mSelectedItem = null;
        }

        private void UpdateList()
        {
            IList<Product> products = ProductGestion.getListProducts();
            if (mProducts == null)
                mProducts = new ObservableCollection<InventoryGestionProductViewModel>();
            else
                mProducts.Clear();
            foreach (Product p in products)
            {
                mProducts.Add(new InventoryGestionProductViewModel(p));
            }
        }

        #region "Add Product"
        private RelayCommand _addProductCommand;
        public System.Windows.Input.ICommand AddProductCommand
        {
            get
            {
                if (_addProductCommand == null)
                {
                    _addProductCommand = new RelayCommand(
                        () => this.AddProduct(),
                        () => this.CanAddProduct()
                        );
                }
                return _addProductCommand;
            }
        }

        private bool CanAddProduct()
        {
            if (!isAddProduct)
                return true;
            return false;
        }

        private void AddProduct()
        {
            SelectedItem = new InventoryGestionProductViewModel(null);
            isAddProduct = true;
        }
        #endregion

        #region "Modify Product"
        private RelayCommand _modifyProductCommand;
        public System.Windows.Input.ICommand ModifyProductCommand
        {
            get
            {
                if (_modifyProductCommand == null)
                {
                    _modifyProductCommand = new RelayCommand(
                        () => this.ModifyProduct(),
                        () => this.CanModifyProduct()
                        );
                }
                return _modifyProductCommand;
            }
        }

        private bool CanModifyProduct()
        {
            if (isAddProduct || SelectedItem != null)
                return true;
            return false;
        }

        private void ModifyProduct()
        {
            if (isAddProduct)
            {
                ProductGestion.addProduct(SelectedItem.Product);
                isAddProduct = false;
            }
            else
                ProductGestion.updateProduct(SelectedItem.Product);
            UpdateList();
        }
        #endregion

        #region "Delete Product"
        private RelayCommand _deleteProductCommand;
        public System.Windows.Input.ICommand DeleteProductCommand
        {
            get
            {
                if (_deleteProductCommand == null)
                {
                    _deleteProductCommand = new RelayCommand(
                        () => this.DeleteProduct(),
                        () => this.CanDeleteProduct()
                        );
                }
                return _deleteProductCommand;
            }
        }

        private bool CanDeleteProduct()
        {
            if(SelectedItem != null)
                return true;
            return false;
        }

        private void DeleteProduct()
        {
            ProductGestion.removeProduct(SelectedItem.Product);
            UpdateList();
        }
        #endregion
    }
}
