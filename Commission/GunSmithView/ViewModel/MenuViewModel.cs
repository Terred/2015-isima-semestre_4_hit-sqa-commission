﻿using GunSmithBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithView.ViewModel
{
    class MenuViewModel : ViewModelBase
    {
        public Action ShowSellsAction;
        public Action ManageProductsAction;
        public Action ManageSalesPersonsAction;

        public String CurrentMonth
        {
            get
            {
                return SalesGestion.CurrentMonth();
            }
        }

        #region "ShowSells"
        private RelayCommand _showSellsCommand;
        public System.Windows.Input.ICommand ShowSellsCommand
        {
            get
            {
                if (_showSellsCommand == null)
                {
                    _showSellsCommand = new RelayCommand(
                        () => this.ShowSells(),
                        () => this.CanShowSells()
                        );
                }
                return _showSellsCommand;
            }
        }

        private bool CanShowSells()
        {
            return true;
        }

        private void ShowSells()
        {
            if(ShowSellsAction != null)
                ShowSellsAction();
        }

        #endregion

        #region "ManageProducts"
        private RelayCommand _manageProductsCommand;
        public System.Windows.Input.ICommand ManageProductsCommand
        {
            get
            {
                if (_manageProductsCommand == null)
                {
                    _manageProductsCommand = new RelayCommand(
                        () => this.ManageProducts(),
                        () => this.CanManageProducts()
                        );
                }
                return _manageProductsCommand;
            }
        }

        private bool CanManageProducts()
        {
            return true;
        }

        private void ManageProducts()
        {
            if(ManageProductsAction != null)
                ManageProductsAction();
        }

        #endregion

        #region "ManageSalesPersons"
        private RelayCommand _manageSalesPersonsCommand;
        public System.Windows.Input.ICommand ManageSalesPersonsCommand
        {
            get
            {
                if (_manageSalesPersonsCommand == null)
                {
                    _manageSalesPersonsCommand = new RelayCommand(
                        () => this.ManageSalesPersons(),
                        () => this.CanManageSalesPersons()
                        );
                }
                return _manageSalesPersonsCommand;
            }
        }

        private bool CanManageSalesPersons()
        {
            return true;
        }

        private void ManageSalesPersons()
        {
            if(ManageSalesPersonsAction != null)
                ManageSalesPersonsAction();
        }

        #endregion

        internal void reload()
        {
            OnPropertyChanged("CurrentMonth");
        }
    }
}
