﻿using CommunEntities;
using GunSmithBusinessLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithView.ViewModel
{
    class SalesPersonsViewModel : ViewModelBase
    {
        private ObservableCollection<SalesPersonViewModel> mSalesPersons;
        private bool isAddSalesPerson = false;

        public ObservableCollection<SalesPersonViewModel> SalesPerson
        {
            get { return mSalesPersons; }
            private set
            {
                mSalesPersons = value;
                OnPropertyChanged("SalesPerson");
            }
        }

        private SalesPersonViewModel mSelectedItem;
        public SalesPersonViewModel SelectedItem
        {
            get { return mSelectedItem; }
            set
            {
                mSelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public SalesPersonsViewModel() 
        {
            UpdateList();
            mSelectedItem = null;
        }

        private void UpdateList()
        {
            IList<SalesPerson> salesPersons = SalesPersonGestion.getListSalesPersons();
            if (mSalesPersons == null)
                mSalesPersons = new ObservableCollection<SalesPersonViewModel>();
            else
                mSalesPersons.Clear();
            foreach (SalesPerson sp in salesPersons)
            {
                mSalesPersons.Add(new SalesPersonViewModel(sp));
            }
        }

        #region "Add SalesPerson"
        private RelayCommand _addSalesPersonCommand;
        public System.Windows.Input.ICommand AddSalesPersonCommand
        {
            get
            {
                if (_addSalesPersonCommand == null)
                {
                    _addSalesPersonCommand = new RelayCommand(
                        () => this.AddSalesPerson(),
                        () => this.CanAddSalesPerson()
                        );
                }
                return _addSalesPersonCommand;
            }
        }

        private bool CanAddSalesPerson()
        {
            if (!isAddSalesPerson)
                return true;
            return false;
        }

        private void AddSalesPerson()
        {
            SelectedItem = new SalesPersonViewModel(null);
            isAddSalesPerson = true;
        }
        #endregion

        #region "Modify SalesPerson"
        private RelayCommand _modifySalesPersonCommand;
        public System.Windows.Input.ICommand ModifySalesPersonCommand
        {
            get
            {
                if (_modifySalesPersonCommand == null)
                {
                    _modifySalesPersonCommand = new RelayCommand(
                        () => this.ModifySalesPerson(),
                        () => this.CanModifySalesPerson()
                        );
                }
                return _modifySalesPersonCommand;
            }
        }

        private bool CanModifySalesPerson()
        {
            if (isAddSalesPerson || SelectedItem != null)
                return true;
            return false;
        }

        private void ModifySalesPerson()
        {
            if (isAddSalesPerson)
            {
                SalesPersonGestion.addSalesPerson(SelectedItem.SalesPerson);
                isAddSalesPerson = false;
            }
            else
                SalesPersonGestion.updateSalesPerson(SelectedItem.SalesPerson);
            UpdateList();
        }
        #endregion

        #region "Delete SalesPerson"
        private RelayCommand _deleteSalesPersonCommand;
        public System.Windows.Input.ICommand DeleteSalesPersonCommand
        {
            get
            {
                if (_deleteSalesPersonCommand == null)
                {
                    _deleteSalesPersonCommand = new RelayCommand(
                        () => this.DeleteSalesPerson(),
                        () => this.CanDeleteSalesPerson()
                        );
                }
                return _deleteSalesPersonCommand;
            }
        }

        private bool CanDeleteSalesPerson()
        {
            if (SelectedItem != null)
                return true;
            return false;
        }

        private void DeleteSalesPerson()
        {
            SalesPersonGestion.removeSalesPerson(SelectedItem.SalesPerson);
            UpdateList();
        }
        #endregion
    }
}
