﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunEntities;
using GunSmithBusinessLayer;

namespace GunSmithView.ViewModel
{
    class ConnectionViewModel : ViewModelBase
    {
        public String ID { get; set; }
        public SecureString SecurePassword
        {
            private get;
            set;
        }

        public event Action AuthentifiedAction;

        private RelayCommand _connectCommand;
        public System.Windows.Input.ICommand ConnectCommand
        {
            get
            {
                if (_connectCommand == null)
                {
                    _connectCommand = new RelayCommand(
                        () => this.Connect(),
                        () => this.CanConnect()
                        );
                }
                return _connectCommand;
            }
        }

        private bool CanConnect()
        {
            if (ID == null || SecurePassword == null)
                return false;
            return (ID.Length != 0 & SecurePassword.Length != 0);
        }

        private void Connect()
        {
            if (Authentification.authentifyGunSmith(ID, Services.EncrypteSecurePassword(SecurePassword)))
            {
                if(AuthentifiedAction != null)
                    AuthentifiedAction();
            }
            else
            {
                MessageBox.Show("Wrong password");
            }
        }
    }
}
