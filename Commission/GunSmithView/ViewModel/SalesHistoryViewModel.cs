﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CommunEntities;
using GunSmithBusinessLayer;
using GunSmithEntities;

namespace GunSmithView.ViewModel
{
    class SalesHistoryViewModel : ViewModelBase
    {
        private ObservableCollection<MonthReport> mMonthReports;

        public ObservableCollection<MonthReport> MonthReports
        {
            get { return mMonthReports; }
            private set
            {
                mMonthReports = value;
                OnPropertyChanged("MonthReports");
            }
        }

        private MonthReport mSelectedItem;
        public MonthReport SelectedItem
        {
            get { return mSelectedItem; }
            set
            {
                mSelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public SalesHistoryViewModel()
        {
            UpdateList();
            mSelectedItem = null;
        }

        private void UpdateList()
        {
            if (mMonthReports == null)
                mMonthReports = new ObservableCollection<MonthReport>();
            else
                mMonthReports.Clear();
            foreach (MonthReport m in SalesGestion.getListMonthReports())
            {
                mMonthReports.Add(m);
            }
        
        }
    }
}
