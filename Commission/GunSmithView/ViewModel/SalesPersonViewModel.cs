﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithView.ViewModel
{
    class SalesPersonViewModel : ViewModelBase
    {
        public SalesPerson SalesPerson { get; private set; }

        public SalesPersonViewModel(SalesPerson sal)
        {
            if (sal != null)
                SalesPerson = sal;
            else
                SalesPerson = new SalesPerson();
        }

        public String Name
        {
            get { return SalesPerson.Name; }
            set
            {
                if (value == SalesPerson.Name) return;
                SalesPerson.Name = value;
                base.OnPropertyChanged("Name");
            }
        }

        public String ID
        {
            get { return SalesPerson.ID; }
        }

        public SecureString SecurePassword
        {
            set
            {
                SalesPerson.EncryptedPassword = Services.EncrypteSecurePassword(value);
            }
        }
    }
}
