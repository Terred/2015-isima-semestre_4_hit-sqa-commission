﻿using SalesPersonView.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesPersonView
{
    /// <summary>
    /// Logique d'interaction pour Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
            this.DataContext = new MenuViewModel();
            ((MenuViewModel)this.DataContext).ShowSellsAction += () => { SalesHistory win = new SalesHistory(); win.Show(); };
            ((MenuViewModel)this.DataContext).ChangeCityAction += () => { ChangeCity win = new ChangeCity(); win.Show(); };
            ((MenuViewModel)this.DataContext).ShowInventoryAction += () => { Inventory win = new Inventory(); win.Show(); };
            ((MenuViewModel)this.DataContext).NewSaleAction += () => { NewSale win = new NewSale(); win.Show(); };
            ((MenuViewModel)this.DataContext).EndMonthAction += () => { EndMonth win = new EndMonth(); win.Show(); };
        }

        private void Window_Focus(object sender, EventArgs e)
        {
            ((MenuViewModel)this.DataContext).reload();
        }
    }
}
