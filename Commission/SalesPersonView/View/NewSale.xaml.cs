﻿using SalesPersonView.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesPersonView
{
    /// <summary>
    /// Logique d'interaction pour New_Sale.xaml
    /// </summary>
    public partial class NewSale : Window
    {
        public NewSale()
        {
            InitializeComponent();
            this.DataContext = new NewSaleViewModel();
            ((NewSaleViewModel)this.DataContext).NewSaleAction += () => { Close(); };
            ((NewSaleViewModel)this.DataContext).CancelAction += () => { Close(); };
        }
    }
}
