﻿using SalesPersonView.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesPersonView
{
    /// <summary>
    /// Logique d'interaction pour Finish_Month.xaml
    /// </summary>
    public partial class EndMonth : Window
    {
        public EndMonth()
        {
            InitializeComponent();
            this.DataContext = new EndMonthViewModel();
            ((EndMonthViewModel)this.DataContext).NextedMonthAction += () => { Close(); };
            ((EndMonthViewModel)this.DataContext).CancelAction += () => { Close(); };
        }
    }
}
