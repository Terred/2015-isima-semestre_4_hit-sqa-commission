﻿using SalesPersonView.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesPersonView
{
    /// <summary>
    /// Logique d'interaction pour Change_City.xaml
    /// </summary>
    public partial class ChangeCity : Window
    {
        public ChangeCity()
        {
            InitializeComponent();
            this.DataContext = new ChangeCityViewModel();
            ((ChangeCityViewModel)this.DataContext).ChangedCityAction += () => { Close(); };
            ((ChangeCityViewModel)this.DataContext).CancelAction += () => { Close(); };
        }
    }
}
