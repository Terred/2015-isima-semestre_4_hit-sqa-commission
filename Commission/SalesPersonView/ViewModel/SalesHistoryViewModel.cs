﻿using SalesPersonBusinessLayer;
using SalesPersonEntities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class SalesHistoryViewModel : ViewModelBase
    {
        private ObservableCollection<MonthSellViewModel> mMonthReports;

        public ObservableCollection<MonthSellViewModel> MonthReports
        {
            get { return mMonthReports; }
            private set
            {
                mMonthReports = value;
                OnPropertyChanged("MonthReports");
            }
        }

        public SalesHistoryViewModel()
        {
            UpdateList();
        }

        private void UpdateList()
        {
            if (mMonthReports == null)
                mMonthReports = new ObservableCollection<MonthSellViewModel>();
            else
                mMonthReports.Clear();
            foreach (MonthSell m in SalesGestion.getListMonthSells())
            {
                mMonthReports.Add(new MonthSellViewModel(m));
            }

        }
    }
}
