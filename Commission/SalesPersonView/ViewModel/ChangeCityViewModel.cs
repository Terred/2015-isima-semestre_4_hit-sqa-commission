﻿using SalesPersonBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class ChangeCityViewModel : ViewModelBase
    {
        public event Action ChangedCityAction;
        public event Action CancelAction;

        private String mCity;
        public String City
        {
            get
            {
                return mCity;
            }
            set
            {
                mCity = value;
                OnPropertyChanged("City");
            }
        }

        public String CurrentCity
        {
            get
            {
                if(SalesGestion.CurrentCity() != null)
                    return (SalesGestion.CurrentCity()).Name;
                else
                    return "Unknow";
            }
        }

        #region "Confirm"
        private RelayCommand _confirmCommand;
        public System.Windows.Input.ICommand ConfirmCommand
        {
            get
            {
                if (_confirmCommand == null)
                {
                    _confirmCommand = new RelayCommand(
                        () => this.Confirm(),
                        () => this.CanConfirm()
                        );
                }
                return _confirmCommand;
            }
        }

        private bool CanConfirm()
        {
            return true;
        }

        private void Confirm()
        {
            SalesGestion.updateCurrentCity(City);
            if (ChangedCityAction != null)
                ChangedCityAction();
        }
        #endregion

        #region "Cancel"
        private RelayCommand _cancelCommand;
        public System.Windows.Input.ICommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        () => this.Cancel(),
                        () => this.CanCancel()
                        );
                }
                return _cancelCommand;
            }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void Cancel()
        {
            if (CancelAction != null)
                CancelAction();
        }
        #endregion
    }
}
