﻿using SalesPersonBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class MenuViewModel : ViewModelBase
    {
        public String SalesPersonName
        {
            get
            {
                return SalesGestion.SalesPersonName();
            }
        }

        public String CurrentCity
        {
            get
            {
                if(SalesGestion.CurrentCity() != null)
                    return (SalesGestion.CurrentCity()).Name;
                else
                    return "Unknow";
            }
        }

        public String CurrentMonth
        {
            get
            {
                return SalesGestion.CurrentMonth();
            }
        }

        public Action ShowSellsAction;
        public Action ChangeCityAction;
        public Action ShowInventoryAction;
        public Action NewSaleAction;
        public Action EndMonthAction;

        #region "ShowSells"
        private RelayCommand _showSellsCommand;
        public System.Windows.Input.ICommand ShowSellsCommand
        {
            get
            {
                if (_showSellsCommand == null)
                {
                    _showSellsCommand = new RelayCommand(
                        () => this.ShowSells(),
                        () => this.CanShowSells()
                        );
                }
                return _showSellsCommand;
            }
        }

        private bool CanShowSells()
        {
            return true;
        }

        private void ShowSells()
        {
            if (ShowSellsAction != null)
                ShowSellsAction();
        }
        #endregion

        #region "Change City"
        private RelayCommand _changeCityCommand;
        public System.Windows.Input.ICommand ChangeCityCommand
        {
            get
            {
                if (_changeCityCommand == null)
                {
                    _changeCityCommand = new RelayCommand(
                        () => this.ChangeCity(),
                        () => this.CanChangeCity()
                        );
                }
                return _changeCityCommand;
            }
        }

        private bool CanChangeCity()
        {
            return true;
        }

        private void ChangeCity()
        {
            if (ChangeCityAction != null)
                ChangeCityAction();
        }
        #endregion

        #region "Show Inventory"
        private RelayCommand _showInventoryCommand;
        public System.Windows.Input.ICommand ShowInventoryCommand
        {
            get
            {
                if (_showInventoryCommand == null)
                {
                    _showInventoryCommand = new RelayCommand(
                        () => this.ShowInventory(),
                        () => this.CanShowInventory()
                        );
                }
                return _showInventoryCommand;
            }
        }

        private bool CanShowInventory()
        {
            return true;
        }

        private void ShowInventory()
        {
            if (ShowInventoryAction != null)
                ShowInventoryAction();
        }
        #endregion

        #region "New Sale"
        private RelayCommand _newSaleCommand;
        public System.Windows.Input.ICommand NewSaleCommand
        {
            get
            {
                if (_newSaleCommand == null)
                {
                    _newSaleCommand = new RelayCommand(
                        () => this.NewSale(),
                        () => this.CanNewSale()
                        );
                }
                return _newSaleCommand;
            }
        }

        private bool CanNewSale()
        {
            return true;
        }

        private void NewSale()
        {
            if (NewSaleAction != null)
                NewSaleAction();
        }
        #endregion

        #region "End Month"
        private RelayCommand _endMonthCommand;
        public System.Windows.Input.ICommand EndMonthCommand
        {
            get
            {
                if (_endMonthCommand == null)
                {
                    _endMonthCommand = new RelayCommand(
                        () => this.EndMonth(),
                        () => this.CanEndMonth()
                        );
                }
                return _endMonthCommand;
            }
        }

        private bool CanEndMonth()
        {
            return true;
        }

        private void EndMonth()
        {
            if (EndMonthAction != null)
                EndMonthAction();
        }
        #endregion

        internal void reload()
        {
            OnPropertyChanged("CurrentCity");
            OnPropertyChanged("CurrentMonth");
        }
    }
}
