﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class ProductViewModel : ViewModelBase
    {
        public Product Product { get; private set; }

        public ProductViewModel(Product product)
        {
            if (product != null)
                Product = product;
            else
                Product = new Product();
        }

        public String Name
        {
            get { return Product.Name; }
        }

        public float Price
        {
            get { return Product.Price; }
        }

        public int Amount
        {
            get { return Product.Amount; }
        }

        public bool SaleMandatory
        {
            get { return Product.SaleMandatory; }
        }
    }
}
