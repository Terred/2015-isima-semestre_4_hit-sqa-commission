﻿using CommunEntities;
using SalesPersonBusinessLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class ProductsViewModel : ViewModelBase
    {
        private ObservableCollection<ProductViewModel> mProducts;
        public ObservableCollection<ProductViewModel> Products
        {
            get { return mProducts; }
            private set
            {
                mProducts = value;
                OnPropertyChanged("Products");
            }
        }

        private ProductViewModel mSelectedItem;
        public ProductViewModel SelectedItem
        {
            get { return mSelectedItem; }
            set
            {
                mSelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public ProductsViewModel() 
        {
            UpdateList();
            mSelectedItem = null;
        }

        private void UpdateList()
        {
            IList<Product> products = ProductGestion.getListProducts();
            if (mProducts == null)
                mProducts = new ObservableCollection<ProductViewModel>();
            else
                mProducts.Clear();
            foreach (Product p in products)
            {
                mProducts.Add(new ProductViewModel(p));
            }
            OnPropertyChanged("Products");
        }

        #region "Fill products"
        private RelayCommand _fillProductsCommand;
        public System.Windows.Input.ICommand FillProductsCommand
        {
            get
            {
                if (_fillProductsCommand == null)
                {
                    _fillProductsCommand = new RelayCommand(
                        () => this.FillProduct(),
                        () => this.CanFillProduct()
                        );
                }
                return _fillProductsCommand;
            }
        }

        private bool CanFillProduct()
        {
            return true;
        }

        private void FillProduct()
        {
            ProductGestion.fillInventory();
            UpdateList();
        }
        #endregion
    }
}
