﻿using SalesPersonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunEntities;
using System.Collections.ObjectModel;
using SalesPersonBusinessLayer;

namespace SalesPersonView.ViewModel
{
    class SellProductUpdateViewModel : ViewModelBase
    {
        public SellProduct SellProduct { get; private set; }

        public SellProductUpdateViewModel()
        {
            SellProduct = new SellProduct();
        }

        public SellProductUpdateViewModel(SellProduct product)
        {
            if (product != null)
                SellProduct = product;
            else
                SellProduct = new SellProduct();
        }

        public Product Product
        {
            get
            {
                return SellProduct.Product;
            }
            set
            {
                SellProduct.Product = value;
                OnPropertyChanged("Product");
                OnPropertyChanged("Price");
            }
        }

        public int Quantity
        {
            set { SellProduct.Quantity = value; OnPropertyChanged("Quantity"); OnPropertyChanged("Price"); }
            get { return SellProduct.Quantity; }
        }

        public float Price
        {
            get { if (SellProduct.Product != null) return SellProduct.Product.Price * Quantity; else return 0; }
        }

        private static ObservableCollection<Product> mProducts = null;
        public static ObservableCollection<Product> Products
        {
            get
            {
                if (mProducts == null)
                {
                    IList<Product> products = ProductGestion.getListProducts();
                    mProducts = new ObservableCollection<Product>();
                    foreach (Product p in products)
                    {
                        mProducts.Add(p);
                    }
                }
                return mProducts;
            }
        }
    }
}
