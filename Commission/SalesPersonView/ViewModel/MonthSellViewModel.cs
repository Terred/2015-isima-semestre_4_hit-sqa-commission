﻿using SalesPersonBusinessLayer;
using SalesPersonEntities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class MonthSellViewModel : ViewModelBase
    {
        public MonthSell MonthSell { get; set; }

        public MonthSellViewModel(MonthSell monthSell)
        {
            MonthSell = monthSell;
            totalIncome = SalesGestion.totalIncomeOfAMonthSell(MonthSell);
        }

        public String Month { get { return MonthSell.Month.ToString("Y", DateTimeFormatInfo.InvariantInfo); } }

        public String EndMonth { get { return MonthSell.EndMonth.ToString("Y", DateTimeFormatInfo.InvariantInfo); } }

        private float totalIncome;
        public float TotalIncome
        {
            get
            {
                return totalIncome;
            }
        }

        public float Commission { get { return MonthSell.Commission; } }
    }
}
