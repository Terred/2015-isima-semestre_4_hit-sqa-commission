﻿using SalesPersonBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommunEntities;
using System.Net.Sockets;

namespace SalesPersonView.ViewModel
{
    class ConnectionViewModel : ViewModelBase
    {
        public String ID { get; set; }
        public SecureString Password { private get; set; }
        public String IP { get; set; }

        public ConnectionViewModel()
        {
            IP = "127.0.0.1";
            ID = "3";
        }

        public event Action AuthentifiedAction;

        private RelayCommand _connectCommand;
        public System.Windows.Input.ICommand ConnectCommand
        {
            get
            {
                if (_connectCommand == null)
                {
                    _connectCommand = new RelayCommand(
                        () => this.Connect(),
                        () => this.CanConnect()
                        );
                }
                return _connectCommand;
            }
        }

        private bool CanConnect()
        {
            if (ID == null || Password == null || IP == null)
                return false;
            return (ID.Length != 0 & Password.Length != 0 & IP.Length != 0);
        }

        private void Connect()
        {
            try
            {
                Client.createClient(IP);
                Authentification.authentifySalesPerson(ID, Services.EncrypteSecurePassword(Password));
                AuthentifiedAction();
            }
            catch (SocketException)
            {
                MessageBox.Show("Error with the server, try again !");
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
