﻿using CommunEntities;
using SalesPersonBusinessLayer;
using SalesPersonEntities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SalesPersonView.ViewModel
{
    class SellUpdateViewModel : ViewModelBase
    {
        public DateTime Date { get; set; }

        private ObservableCollection<SellProductUpdateViewModel> mSellProductsViewModel;
        public ObservableCollection<SellProductUpdateViewModel> SellProductsViewModel
        {
            get { return mSellProductsViewModel; }
            private set
            {
                mSellProductsViewModel = value;
                OnPropertyChanged("SellProducts");
            }
        }

        public ObservableCollection<SellProduct> SellProducts
        {
            get
            {
                ObservableCollection<SellProduct> sellProducts = new ObservableCollection<SellProduct>();
                foreach (SellProductUpdateViewModel s in SellProductsViewModel)
                {
                    sellProducts.Add(s.SellProduct);
                }
                return sellProducts;
            }
        }

        public SellUpdateViewModel() 
        {
            mSellProductsViewModel = new ObservableCollection<SellProductUpdateViewModel>();
        }
    }
}
