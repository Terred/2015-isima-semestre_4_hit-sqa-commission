﻿using SalesPersonBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonView.ViewModel
{
    class NewSaleViewModel : ViewModelBase
    {
        public event Action NewSaleAction;
        public event Action CancelAction;

        public SellUpdateViewModel SellUpdate { get; set; }

        public NewSaleViewModel()
        {
            SellUpdate = new SellUpdateViewModel();
            SellUpdate.Date = SalesGestion.lastDateOfCurrentMonthSell();
        }

        #region "Confirm"
        private RelayCommand _confirmCommand;
        public System.Windows.Input.ICommand ConfirmCommand
        {
            get
            {
                if (_confirmCommand == null)
                {
                    _confirmCommand = new RelayCommand(
                        () => this.Confirm(),
                        () => this.CanConfirm()
                        );
                }
                return _confirmCommand;
            }
        }

        private bool CanConfirm()
        {
            return true;
        }

        private void Confirm()
        {
            SalesGestion.addSale(SellUpdate.SellProducts, SellUpdate.Date);
            if (NewSaleAction != null)
                NewSaleAction();
        }
        #endregion

        #region "Cancel"
        private RelayCommand _cancelCommand;
        public System.Windows.Input.ICommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        () => this.Cancel(),
                        () => this.CanCancel()
                        );
                }
                return _cancelCommand;
            }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void Cancel()
        {
            if (CancelAction != null)
                CancelAction();
        }
        #endregion
    }
}
