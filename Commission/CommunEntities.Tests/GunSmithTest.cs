// <copyright file="GunSmithTest.cs">Copyright �  2015</copyright>

using System;
using CommunEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommunEntities
{
    [TestClass]
    [PexClass(typeof(GunSmith))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class GunSmithTest
    {
        [PexMethod]
        public GunSmith Constructor(
            string id,
            string name,
            string encryptedPassword
        )
        {
            GunSmith target = new GunSmith(id, name, encryptedPassword);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode GunSmithTest.Constructor(String, String, String)
        }
    }
}
