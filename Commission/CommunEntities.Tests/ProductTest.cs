// <copyright file="ProductTest.cs">Copyright �  2015</copyright>

using System;
using CommunEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommunEntities
{
    [TestClass]
    [PexClass(typeof(Product))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ProductTest
    {
        [PexMethod]
        public Product Constructor02()
        {
            Product target = new Product();
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductTest.Constructor02()
        }
        [PexMethod]
        public Product Constructor(
            string name,
            int amount,
            float price,
            bool saleMandatory,
            GunSmith gunSmith
        )
        {
            Product target = new Product(name, amount, price, saleMandatory, gunSmith);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductTest.Constructor(String, Int32, Single, Boolean, GunSmith)
        }
        [PexMethod]
        public Product Constructor01(
            string name,
            int amount,
            float price,
            bool saleMandatory
        )
        {
            Product target = new Product(name, amount, price, saleMandatory);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductTest.Constructor01(String, Int32, Single, Boolean)
        }
    }
}
