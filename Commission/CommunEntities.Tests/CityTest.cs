// <copyright file="CityTest.cs">Copyright �  2015</copyright>

using System;
using CommunEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommunEntities
{
    [TestClass]
    [PexClass(typeof(City))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class CityTest
    {
        [PexMethod]
        public City Constructor(string name)
        {
            City target = new City(name);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode CityTest.Constructor(String)
        }
    }
}
