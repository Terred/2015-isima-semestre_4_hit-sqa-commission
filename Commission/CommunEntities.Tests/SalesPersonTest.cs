// <copyright file="SalesPersonTest.cs">Copyright �  2015</copyright>

using System;
using CommunEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommunEntities
{
    [TestClass]
    [PexClass(typeof(SalesPerson))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class SalesPersonTest
    {
        [PexMethod]
        public SalesPerson Constructor01()
        {
            SalesPerson target = new SalesPerson();
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonTest.Constructor01()
        }
        [PexMethod]
        public SalesPerson Constructor(
            string id,
            string name,
            string encryptedPassword,
            GunSmith gunSmith
        )
        {
            SalesPerson target = new SalesPerson(id, name, encryptedPassword, gunSmith);
            return target;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonTest.Constructor(String, String, String, GunSmith)
        }
    }
}
