using System.Security;
// <copyright file="ServicesTest.cs">Copyright �  2015</copyright>

using System;
using CommunEntities;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommunEntities
{
    [TestClass]
    [PexClass(typeof(Services))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ServicesTest
    {
        [PexMethod]
        public string EncrypteSecurePassword(SecureString SecurePassword)
        {
            string result = Services.EncrypteSecurePassword(SecurePassword);
            return result;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ServicesTest.EncrypteSecurePassword(SecureString)
        }
    }
}
