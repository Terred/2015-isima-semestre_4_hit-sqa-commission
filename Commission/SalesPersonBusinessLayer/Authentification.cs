﻿using CommunEntities;
using SalesPersonEntities;
using SalesPersonStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonBusinessLayer
{
    public class Authentification
    {
        public static void authentifySalesPerson(String id, String encryptedPassword)
        {
            DalManager.createDalManager();

            AuthentificationPacket packet = new AuthentificationPacket(id, encryptedPassword);
            Packet.Send(packet, Client.Singleton.getStream());
            Packet result = Packet.Receive(Client.Singleton.getStream());
            if (result is AuthentificationValidationPacket)
            {
                DalManager.Singleton.GunSmith = ((AuthentificationValidationPacket)result).GunSmith;
                DalManager.Singleton.SalesPerson = ((AuthentificationValidationPacket)result).SalesPerson;
            }
            else if (result is ErrorPacket)
            {
                throw new Exception(((ErrorPacket)result).Error);
            }
            else
                throw new Exception("Unknown error !");
            ProductGestion.fillInventory();
            DalManager.Singleton.CurrentMonthSell = new MonthSell(DalManager.Singleton.CurrentMonth);
            SellManager.addMonthSell(DalManager.Singleton.CurrentMonthSell);
        }
    }
}
