﻿using CommunEntities;
using SalesPersonStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonBusinessLayer
{
    public class ProductGestion
    {
        public static IList<Product> getListProducts()
        {
            return ProductManager.getListProducts();
        }

        public static void fillInventory()
        {
            AskPacket ask = new AskPacket(AskRequest.Inventory);

            Packet.Send(ask, Client.Singleton.getStream());
            Packet result = Packet.Receive(Client.Singleton.getStream());
            if (result is InventoryPacket)
            {
                foreach (Product p in ((InventoryPacket)result).Products)
                {
                    if (ProductManager.getProductFromName(p.Name) == null)
                        ProductManager.addProduct(p);
                    else
                        ProductManager.updateProduct(p);
                }
            }
            else
                throw new Exception("Error");
        }
    }
}
