﻿using CommunEntities;
using SalesPersonEntities;
using SalesPersonStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonBusinessLayer
{
    public class SalesGestion
    {
        public static String SalesPersonName()
        {
            return DalManager.Singleton.SalesPerson.Name;
        }

        public static City CurrentCity()
        {
            return DalManager.Singleton.CurrentCity;
        }

        public static String CurrentMonth()
        {
            return DalManager.Singleton.CurrentMonth.ToString("Y", DateTimeFormatInfo.InvariantInfo);
        }

        public static void updateCurrentCity(string City)
        {
            nextCity();
            DalManager.Singleton.CurrentCity = new City(City);
            DalManager.Singleton.CurrentCitySell = new CitySell(DalManager.Singleton.CurrentCity, DalManager.Singleton.CurrentMonthSell);
            CityManager.addCity(DalManager.Singleton.CurrentCity);
            SellManager.addCitySell(DalManager.Singleton.CurrentCitySell);
        }

        public static void nextCity()
        {
            if (DalManager.Singleton.CurrentCitySell != null)
                SendCityReport(DalManager.Singleton.CurrentCitySell);
            DalManager.Singleton.CurrentCity = null;
            DalManager.Singleton.CurrentCitySell = null;
        }

        public static void nextMonth()
        {
            nextCity();
            DalManager.Singleton.CurrentMonth = DalManager.Singleton.CurrentMonth.AddMonths(1);
            DalManager.Singleton.CurrentMonthSell.EndMonth = lastDateOfCurrentMonthSell();
            sendNewMonth();
            DalManager.Singleton.CurrentMonthSell = new MonthSell(DalManager.Singleton.CurrentMonth);
            SellManager.addMonthSell(DalManager.Singleton.CurrentMonthSell);
        }

        public static void addSale(IList<SellProduct> sellProducts, DateTime dateTime)
        {
            Sell sell = new Sell(dateTime, DalManager.Singleton.CurrentCitySell);
            SellManager.addSell(sell);
            foreach (SellProduct p in sellProducts)
            {
                p.Sell = sell;
                SellManager.addSellProduct(p);
            }
        }

        public static void sendNewMonth()
        {
            EndMonthPacket endMonth = new EndMonthPacket(DalManager.Singleton.CurrentMonthSell.EndMonth);
            Packet.Send(endMonth, Client.Singleton.getStream());

            Packet result = Packet.Receive(Client.Singleton.getStream());
            if (result is CommissionPacket)
            {
                DalManager.Singleton.CurrentMonthSell.Commission = ((CommissionPacket)result).Commission;
            }
            else
                throw new Exception("Error");
        }

        public static void SendCityReport(CitySell citySell)
        {
            Dictionary<Product, int> mProducts = new Dictionary<Product, int>();
            foreach(SellProduct sp in SellManager.getSellProductsFromCitySell(citySell))
            {
                mProducts[sp.Product] = sp.Quantity;
            }
            CityReportPacket cityReport = new CityReportPacket(citySell.City, mProducts);
            Packet.Send(cityReport, Client.Singleton.getStream());
        }

        public static IEnumerable<MonthSell> getListMonthSells()
        {
            return SellManager.getListMonthSells();
        }

        public static float totalIncomeOfAMonthSell(MonthSell monthsell)
        {
            float totalIncome = 0;

            foreach (SellProduct sp in SellManager.getSellProductsFromMonthSell(monthsell))
            {
                totalIncome += sp.Product.Price * sp.Quantity;
            }

            return totalIncome;
        }

        public static DateTime lastDateOfCurrentMonthSell()
        {
            List<Sell> sells = SellManager.getSellFromMonthSell(DalManager.Singleton.CurrentMonthSell);
            if(sells != null & sells.Count > 0)
                return sells.Select(sell => sell.Date).Max();
            return DalManager.Singleton.CurrentMonthSell.Month;
        }
    }
}
