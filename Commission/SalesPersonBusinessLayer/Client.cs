﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace SalesPersonBusinessLayer
{
    public class Client
    {
        private static readonly int mPort = 5500;
        private TcpClient ClientServer { get; set; }
        public static Client Singleton { get; private set; }

        public static void createClient(String ipAdress)
        {
            if(Singleton == null)
                Singleton = new Client(ipAdress);
        }

        private Client(String ipAddress)
        {
            ClientServer = new TcpClient();
            ClientServer.Connect(ipAddress, mPort);
        }

        public NetworkStream getStream()
        {
            return ClientServer.GetStream();
        }
    }
}
