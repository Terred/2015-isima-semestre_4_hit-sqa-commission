﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    public enum AskRequest
    {
        Commission,
        Inventory
    }

    [Serializable()]
    public class AskPacket : Packet
    {
        public AskRequest Request { get; set; }

        public AskPacket(AskRequest request)
            : base(TypePaquet.Ask)
        {
            Request = request;
        }
    }
}
