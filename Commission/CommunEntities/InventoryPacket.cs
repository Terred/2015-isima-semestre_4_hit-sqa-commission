﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class InventoryPacket : Packet
    {
        public IList<Product> Products { get; set; }

        public InventoryPacket(IList<Product> products) : base(TypePaquet.Inventory)
        {
            Products = products;
        }
    }
}
