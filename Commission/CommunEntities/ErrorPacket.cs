﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class ErrorPacket : Packet
    {
        public String Error { get; private set; }

        public ErrorPacket(String error)
            : base(TypePaquet.Error)
        {
            this.Error = error;
        }
    }
}
