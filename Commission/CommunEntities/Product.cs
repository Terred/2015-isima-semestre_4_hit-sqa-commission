﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunEntities
{
    /// <summary>
    /// Product that produce the GunSmith and selled by SalesPerson.
    /// </summary>
    [Serializable()]
    public class Product
    {
        public String Name { get; set; }

        public int QuantityProducedPerMonth { get; set; }

        public int Amount { get; set; }

        public float Price { get; set; }

        public bool SaleMandatory { get; set; }

        public GunSmith GunSmith { get; set; }

        public Product(String name, int amount, float price, bool saleMandatory, GunSmith gunSmith)
        {
            Name = name;
            Amount = amount;
            Price = price;
            SaleMandatory = saleMandatory;
            GunSmith = gunSmith;
        }

        public Product(String name, int amount, float price, bool saleMandatory)
        {
            Name = name;
            Amount = amount;
            Price = price;
            SaleMandatory = saleMandatory;
        }

        public Product()
        {
            Name = "";
            Amount = 0;
            Price = 0;
            SaleMandatory = false;
        }
    }
}
