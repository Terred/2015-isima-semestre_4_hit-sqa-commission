﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    public class Services
    {
        public static string EncrypteSecurePassword(SecureString SecurePassword)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            String encryptedPassword;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(SecurePassword);
                encryptedPassword = Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
            return encryptedPassword;
        }
    }
}
