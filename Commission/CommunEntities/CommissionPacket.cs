﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class CommissionPacket : Packet
    {
        public float Commission { get; set; }

        public CommissionPacket(float commission)
            : base(TypePaquet.Commission)
        {
            Commission = commission;
        }
    }
}
