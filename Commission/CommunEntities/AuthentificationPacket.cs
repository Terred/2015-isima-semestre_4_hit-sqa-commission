﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class AuthentificationPacket : Packet
    {
        public String Id { get; private set; }
        public String EncryptedPassword { get; private set; }

        public AuthentificationPacket(String Id, String encryptedPassword)
            : base(TypePaquet.Authentification)
        {
            this.Id = Id;
            this.EncryptedPassword = encryptedPassword;
        }
    }
}
