﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class AuthentificationValidationPacket : ValidationPacket
    {
        public GunSmith GunSmith { get; set; }
        public SalesPerson SalesPerson { get; set; }

        public AuthentificationValidationPacket(String validateMessage, GunSmith gunSmith, SalesPerson salesPerson) : base(validateMessage)
        {
            GunSmith = gunSmith;
            SalesPerson = salesPerson;
        }
    }
}
