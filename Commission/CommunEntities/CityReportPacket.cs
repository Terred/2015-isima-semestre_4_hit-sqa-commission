﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class CityReportPacket : Packet
    {
        public City City { get; set; }
        public Dictionary<Product, int> ProductSells { get; set; }

        public CityReportPacket(City city, Dictionary<Product, int> productSells) : base(TypePaquet.CityReport)
        {
            City = city;
            ProductSells = productSells;
        }
    }
}
