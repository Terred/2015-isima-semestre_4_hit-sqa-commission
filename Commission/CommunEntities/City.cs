﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunEntities
{
    [Serializable()]
    public class City
    {
        public String Name { get; set; }

        public City(String name)
        {
            Name = name;
        }
    }
}
