﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace CommunEntities
{
    public enum TypePaquet
    {
        Authentification,
        Validation,
        Error,
        CityReport,
        EndMonth,
        Commission,
        Inventory,
        Ask
    }

    [Serializable()]
    public class Packet
    {
        public TypePaquet Type { get; protected set; }

        public Packet(TypePaquet Type)
        {
            this.Type = Type;
        }

        public static void Send(Packet packet, NetworkStream stream)
        {
            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(stream, packet);
            stream.Flush();
        }

        public static Packet Receive(NetworkStream stream)
        {
            Packet p = null;

            BinaryFormatter bf = new BinaryFormatter();
            p = (Packet)bf.Deserialize(stream);

            return p;
        }
    }
}
