﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CommunEntities
{
    [Serializable()]
    public class SalesPerson
    {
        public String ID { get; set; }

        public String EncryptedPassword { get; set; }

        public String Name { get; set; }

        public GunSmith GunSmith { get; set; }

        public SalesPerson(String id, String name, String encryptedPassword, GunSmith gunSmith)
        {
            ID = id;
            EncryptedPassword = encryptedPassword;
            Name = name;
            GunSmith = gunSmith;
        }

        public SalesPerson()
        {
            ID = "";
            EncryptedPassword = "";
            Name = "";
        }
    }
}
