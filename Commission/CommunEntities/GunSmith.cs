﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CommunEntities
{
    [Serializable()]
    public class GunSmith
    {
        public String ID { get; set; }

        public String EncryptedPassword { get; set; }

        public String Name { get; set; }

        public GunSmith(String id, String name, String encryptedPassword)
        {
            ID = id;
            EncryptedPassword = encryptedPassword;
            Name = name;
        }

        /*private List<Product> mProducts = new List<Product>();

        public ReadOnlyCollection<Product> Products
        {
            get
            {
                return new ReadOnlyCollection<Product>(mProducts);
            }
        }

        public void addProduct(Product p)
        {
            mProducts.Add(p);
        }

        public void removeProduct(Product p)
        {
            mProducts.Remove(p);
        }*/
    }
}
