﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class ValidationPacket : Packet
    {
        public String ValidateMessage { get; private set; }

        public ValidationPacket(String validateMessage)
            : base(TypePaquet.Validation)
        {
            this.ValidateMessage = validateMessage;
        }
    }
}
