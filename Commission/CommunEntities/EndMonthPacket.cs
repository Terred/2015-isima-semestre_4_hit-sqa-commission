﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunEntities
{
    [Serializable()]
    public class EndMonthPacket : Packet
    {
        public DateTime EndMonth { get; set; }

        public EndMonthPacket(DateTime endMonth)
            : base(TypePaquet.EndMonth)
        {
            EndMonth = endMonth;
        }
    }
}
