﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommunEntities;

namespace GunSmithEntities
{
    public class CityReport
    {
        public MonthReport MonthReport { get; set; }
        //private Dictionary<Product, int> mSells;
        public float TotalAmount { get; set; }
        public City City { get; set; }

        public CityReport(MonthReport monthReport, float totalAmount, City city)
        {
            MonthReport = monthReport;
            TotalAmount = totalAmount;
            City = city;
        }
    }
}
