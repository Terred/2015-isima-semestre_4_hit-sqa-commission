﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommunEntities;

namespace GunSmithEntities
{
    public class MonthReport
    {
        public SalesPerson Seller { get; set; }
        public DateTime Month { get; set; }
        public DateTime? EndMonth { get; set; }
        public float TotalIncome { get; set; }
        public float Commission { get; set; }

        public MonthReport(SalesPerson seller, DateTime month)
        {
            Seller = seller;
            Month = month;
            EndMonth = null;
            TotalIncome = 0;
            Commission = 0;
        }

        public MonthReport(SalesPerson seller, DateTime month, DateTime? endMonth, float totalIncome, float commission)
        {
            Seller = seller;
            Month = month;
            EndMonth = endMonth;
            TotalIncome = totalIncome;
            Commission = commission;
        }
    }
}
