﻿using CommunEntities;
using GunSmithStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithBusinessLayer
{
    public class ProductGestion
    {
        public static IList<Product> getListProducts()
        {
            return ProductManager.getListProducts();
        }

        public static void addProduct(Product product)
        {
            product.GunSmith = DalManager.Singleton.CurrentGunSmith;
            ProductManager.addProduct(product);
        }

        public static void updateProduct(Product product)
        {
            ProductManager.updateProduct(product);
        }

        public static void removeProduct(Product product)
        {
            ProductManager.removeProduct(product);
        }
    }
}
