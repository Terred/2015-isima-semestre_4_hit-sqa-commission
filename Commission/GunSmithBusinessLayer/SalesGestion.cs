﻿using GunSmithEntities;
using GunSmithStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithBusinessLayer
{
    public class SalesGestion
    {
        public static IList<MonthReport> getListMonthReports()
        {
            return MonthReportManager.getListMonthReports();
        }

        public static float calculCommission(MonthReport monthReport)
        {
            //Test a faire
            float commission = 0;
            if (monthReport.TotalIncome < 1000)
            {
                commission = monthReport.TotalIncome * 10 / 100;
            }
            else if (monthReport.TotalIncome < 1800)
            {
                commission = monthReport.TotalIncome * 15 / 100;
            }
            else
            {
                commission = monthReport.TotalIncome * 20 / 100;
            }
            return commission;
        }

        public static String CurrentMonth()
        {
            List<MonthReport> monthReports = MonthReportManager.getListMonthReports();
            if (monthReports.Count > 0)
            {
                return monthReports.Select(mr => mr.Month).Max().ToString("Y", DateTimeFormatInfo.InvariantInfo);
            }
            return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("Y", DateTimeFormatInfo.InvariantInfo);
        }
    }


}
