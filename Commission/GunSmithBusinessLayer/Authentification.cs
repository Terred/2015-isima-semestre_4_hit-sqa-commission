﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunEntities;
using GunSmithStubDataAccessLayer;

namespace GunSmithBusinessLayer
{
    public class Authentification
    {
        public static bool authentifyGunSmith(String id, String encryptedPassword)
        {
            DalManager.createDalManager();
            GunSmith gun = GunSmithManager.GunSmithFromID(id);
            if (gun != null)
            {
                if (String.Compare(gun.EncryptedPassword, encryptedPassword) == 0)
                {
                    DalManager.Singleton.CurrentGunSmith = gun;
                    Server serv = Server.Singleton;
                    serv.startServer();
                    return true;
                }
            }
            return false;
        }

        public static void unAuthentifyGunSmith()
        {
            if (DalManager.Singleton.CurrentGunSmith != null)
            {
                Server.Singleton.stopServer();
            }
        }

        public static bool authentifySalesPerson(String id, String encryptedPassword)
        {
            SalesPerson sal = SalesPersonManager.SalePersonFromID(id);
            if (sal != null)
            {
                if (String.Compare(sal.EncryptedPassword, encryptedPassword) == 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
