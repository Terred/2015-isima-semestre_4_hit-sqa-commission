﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using CommunEntities;
using GunSmithStubDataAccessLayer;
using GunSmithEntities;

namespace GunSmithBusinessLayer
{
    class ClientHandler
    {
        public TcpClient Client { get; private set; }

        public SalesPerson SalesPerson { get; private set; }

        public ClientHandler(TcpClient client)
        {
            Client = client;
        }

        public void HandleClient(object obj)
        {
            Packet packet;

            try
            {
                authentifySalesPerson();
                MonthReportManager.addMonthReport(new MonthReport(SalesPerson, new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1)));
                while (true)
                {
                    packet = Packet.Receive(Client.GetStream());
                    switch (packet.Type)
                    {
                        case TypePaquet.Ask:
                            AnswerAsk((AskPacket)packet);
                            break;
                        case TypePaquet.CityReport:
                            float totalIncome = 0;
                            foreach(var item in ((CityReportPacket)packet).ProductSells)
                            {
                                totalIncome += ((Product)item.Key).Price * (int)item.Value;
                            }
                            CityReportManager.addCityReport(new CityReport(MonthReportManager.getActualMonthReport(SalesPerson), totalIncome, ((CityReportPacket)packet).City));
                            //TODO UPDATE VRAIMENT MONTH REPORT
                            MonthReportManager.getActualMonthReport(SalesPerson).TotalIncome += totalIncome;
                            //TODO ADD CITY
                            break;
                        case TypePaquet.EndMonth:
                            MonthReport actual = MonthReportManager.getActualMonthReport(SalesPerson);
                            actual.EndMonth = ((EndMonthPacket)packet).EndMonth;
                            actual.Commission = SalesGestion.calculCommission(actual);
                            //TODO UPDATE VRAIMENT MONTH REPORT
                            Packet result = new CommissionPacket(actual.Commission);
                            Packet.Send(result, Client.GetStream());
                            MonthReportManager.addMonthReport(new MonthReport(SalesPerson, actual.Month.AddMonths(1)));
                            break;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void AnswerAsk(AskPacket ask)
        {
            Packet result;
            switch (ask.Request)
            {
                case AskRequest.Inventory:
                    //TODODODODO DOODOOODOODOODODODOODODODOODODODOODODODO
                    IList<Product> products = new List<Product>();
                    foreach (Product p in ProductManager.getListProducts())
                    {
                        products.Add(p);
                    }
                    result = new InventoryPacket(products);
                    Packet.Send(result, Client.GetStream());
                    break;
            }
        }

        private bool authentifySalesPerson()
        {
            Packet packet, result;
            AuthentificationPacket auth;
            bool isAuthentifiate = false;

            while (!isAuthentifiate)
            {
                packet = Packet.Receive(Client.GetStream());
                if (packet is AuthentificationPacket)
                {
                    auth = (AuthentificationPacket)packet;
                    
                    if (Authentification.authentifySalesPerson(auth.Id, auth.EncryptedPassword))
                    {
                        SalesPerson = SalesPersonManager.SalePersonFromID(auth.Id);
                        result = new AuthentificationValidationPacket("Connection accepted", DalManager.Singleton.CurrentGunSmith, SalesPerson);
                        isAuthentifiate = true;
                    }
                    else
                        result = new ErrorPacket("Incorrect login !");
                    Packet.Send(result, Client.GetStream());
                }
            }
            return true;
        }
    }
}
