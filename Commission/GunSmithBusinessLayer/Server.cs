﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;

namespace GunSmithBusinessLayer
{
    public class Server
    {

        private static readonly int mPort = 5500;

        private TcpListener ServerClient { get; set; }
        private Boolean IsRunning { get; set; }
        Thread mThread = null;
        //private Dictionary<String, ClientHandler> clients;
        
        public static Server Singleton {
            get 
            {
                if (_singleton == null)
                    Singleton = new Server();
                return _singleton;
            }
            private set
            {
                _singleton = value;
            }
        }
        private static Server _singleton;

        private Server()
        {
            IsRunning = false;
        }

        public void startServer()
        {
            if (!IsRunning)
            {
                ServerClient = new TcpListener(IPAddress.Any, mPort);
                ServerClient.Start();

                mThread = new Thread(new ParameterizedThreadStart(this.WaitClients));
                mThread.Start();

                IsRunning = true;
            }
        }

        public void stopServer()
        {
            if (IsRunning)
            {
                if (mThread != null & mThread.IsAlive)
                {
                    IsRunning = false;
                    try
                    {
                        if (!mThread.Join(1000))
                        {
                            mThread.Abort();
                        }
                    }
                    catch (ThreadStartException)
                    {}
                    mThread = null;
                }
                ServerClient.Stop();
            }
        }

        public void WaitClients(Object obj)
        {
            while (IsRunning)
            {
                TcpClient newClient = ServerClient.AcceptTcpClient();

                ClientHandler client = new ClientHandler(newClient);

                Thread t = new Thread(new ParameterizedThreadStart(client.HandleClient));
                t.Start();
            }
        }
    }
}
