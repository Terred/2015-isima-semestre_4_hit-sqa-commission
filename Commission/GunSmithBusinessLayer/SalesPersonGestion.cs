﻿using CommunEntities;
using GunSmithStubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithBusinessLayer
{
    public class SalesPersonGestion
    {
        public static IList<SalesPerson> getListSalesPersons()
        {
            return SalesPersonManager.getListSalesPersons();
        }

        public static void addSalesPerson(SalesPerson salesPerson)
        {
            salesPerson.GunSmith = DalManager.Singleton.CurrentGunSmith;
            SalesPersonManager.addSalesPerson(salesPerson);
        }

        public static void updateSalesPerson(SalesPerson salesPerson)
        {
            SalesPersonManager.updateSalesPerson(salesPerson);
        }

        public static void removeSalesPerson(SalesPerson salesPerson)
        {
            SalesPersonManager.removeSalesPerson(salesPerson);
        }
    }
}
