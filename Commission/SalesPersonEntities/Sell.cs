﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommunEntities;

namespace SalesPersonEntities
{
    public class Sell
    {
        public DateTime Date { get; set; }
        public CitySell CitySell { get; set; }

        public Sell(DateTime date, CitySell citySell)
        {
            Date = date;
            CitySell = citySell;
        }
    }
}
