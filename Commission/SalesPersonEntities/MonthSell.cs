﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesPersonEntities
{
    public class MonthSell
    {
        public DateTime Month { get; set; }
        public DateTime EndMonth { get; set; }
        public float Commission { get; set; }

        public MonthSell(DateTime month)
        {
            Month = month;
        }
    }
}
