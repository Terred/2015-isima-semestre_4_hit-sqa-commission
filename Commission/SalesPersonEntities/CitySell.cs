﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommunEntities;

namespace SalesPersonEntities
{
    public class CitySell
    {
        public City City { get; set; }
        public MonthSell MonthSell { get; set; }

        public CitySell(City city, MonthSell monthSell)
        {
            City = city;
            MonthSell = monthSell;
        }
    }
}
