﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonEntities
{
    public class SellProduct
    {
        public Sell Sell { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }

        public SellProduct(Sell sell, Product product, int quantity)
        {
            Sell = sell;
            Product = product;
            Quantity = quantity;
        }

        public SellProduct()
        {
            Sell = null;
            Product = null;
            Quantity = 0;
        }
    }
}
