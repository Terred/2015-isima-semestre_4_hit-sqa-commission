﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunEntities;
using SalesPersonEntities;

namespace SalesPersonStubDataAccessLayer
{
    public class DalManager
    {
        private static DalManager mSingleton;

        private DalManager()
        {
            GunSmith = null;
            SalesPerson = null;
            CurrentMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            Products = new List<Product>();
            Cities = new List<City>();
            MonthSells = new List<MonthSell>();
            CitySells = new List<CitySell>();
            Sells = new List<Sell>();
            SellProducts = new List<SellProduct>();
        }

        public static void createDalManager()
        {
            if (mSingleton == null)
                Singleton = new DalManager();
        }

        public static DalManager Singleton
        {
            get
            {
                return mSingleton;
            }
            private set
            {
                mSingleton = value;
            }
        }

        public GunSmith GunSmith { get; set; }
        public SalesPerson SalesPerson { get; set; }
        public City CurrentCity { get; set; }
        public CitySell CurrentCitySell { get; set; }
        public MonthSell CurrentMonthSell { get; set; }
        public DateTime CurrentMonth { get; set; }

        public List<Product> Products { get; set; }
        public List<City> Cities { get; set; }
        public List<MonthSell> MonthSells { get; set; }
        public List<CitySell> CitySells { get; set; }
        public List<Sell> Sells { get; set; }
        public List<SellProduct> SellProducts { get; set; }
    }
}
