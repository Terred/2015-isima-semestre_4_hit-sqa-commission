﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonStubDataAccessLayer
{
    public class ProductManager
    {
        public static IList<Product> getListProducts()
        {
            return DalManager.Singleton.Products/*.Where(prod => prod.GunSmith == DalManager.Singleton.GunSmith)*/.ToList();
        }

        public static Product getProductFromName(String name)
        {
            return DalManager.Singleton.Products.Where(prod => prod.Name == name).FirstOrDefault();
        }

        public static void updateProduct(Product product)
        {
            Product mProduct = DalManager.Singleton.Products.Where(prod => prod.Name == product.Name).FirstOrDefault();
            mProduct.Price = product.Price;
            mProduct.QuantityProducedPerMonth = product.QuantityProducedPerMonth;
            mProduct.SaleMandatory = product.SaleMandatory;
            mProduct.Amount = mProduct.Amount;
            mProduct.GunSmith = product.GunSmith;
            mProduct.Name = product.Name;
        }

        public static void addProduct(Product product)
        {
            DalManager.Singleton.Products.Add(product);
        }
    }
}
