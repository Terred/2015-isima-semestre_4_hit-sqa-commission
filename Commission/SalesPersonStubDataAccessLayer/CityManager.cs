﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonStubDataAccessLayer
{
    public class CityManager
    {
        public static void addCity(City city)
        {
            if (!DalManager.Singleton.Cities.Contains(city))
                DalManager.Singleton.Cities.Add(city);
        }
    }
}
