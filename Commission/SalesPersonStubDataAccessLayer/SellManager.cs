﻿using SalesPersonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesPersonStubDataAccessLayer
{
    public class SellManager
    {
        public static void addSell(SalesPersonEntities.Sell sell)
        {
            DalManager.Singleton.Sells.Add(sell);
        }

        public static void addSellProduct(SalesPersonEntities.SellProduct sellProduct)
        {
            DalManager.Singleton.SellProducts.Add(sellProduct);
        }

        public static void addCitySell(SalesPersonEntities.CitySell citySell)
        {
            DalManager.Singleton.CitySells.Add(citySell);
        }

        public static void addMonthSell(SalesPersonEntities.MonthSell monthSell)
        {
            DalManager.Singleton.MonthSells.Add(monthSell);
        }

        public static List<SellProduct> getSellProductsFromSell(Sell sell)
        {
            return DalManager.Singleton.SellProducts.Where(sp => sp.Sell == sell).ToList();
        }

        public static List<SellProduct> getSellProductsFromMonthSell(MonthSell monthSell)
        {
            List<SellProduct> sellProducts = new List<SellProduct>();
            foreach(CitySell cs in getCitySellFromMonthSell(monthSell))
            {
                foreach(Sell s in getSellFromCitySell(cs))
                {
                    sellProducts.AddRange(getSellProductsFromSell(s));
                }
            }
            return sellProducts;
        }

        public static List<SellProduct> getSellProductsFromCitySell(CitySell citySell)
        {
            List<SellProduct> sellProducts = new List<SellProduct>();
            foreach (Sell s in getSellFromCitySell(citySell))
            {
                sellProducts.AddRange(getSellProductsFromSell(s));
            }
            return sellProducts;
        }

        public static List<Sell> getSellFromCitySell(CitySell citySell)
        {
            return DalManager.Singleton.Sells.Where(s => s.CitySell == citySell).ToList();
        }

        public static List<CitySell> getCitySellFromMonthSell(MonthSell monthSell)
        {
            return DalManager.Singleton.CitySells.Where(cs => cs.MonthSell == monthSell).ToList();
        }

        public static List<Sell> getSellFromMonthSell(MonthSell monthSell)
        {
            List<Sell> sells = new List<Sell>();
            foreach (CitySell cs in getCitySellFromMonthSell(monthSell))
            {
                sells.AddRange(getSellFromCitySell(cs));
            }
            return sells;
        }

        public static List<MonthSell> getListMonthSells()
        {
            return DalManager.Singleton.MonthSells.ToList();
        }
    }
}
