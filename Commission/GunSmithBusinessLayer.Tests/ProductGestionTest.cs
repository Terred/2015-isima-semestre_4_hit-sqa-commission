using System.Collections.Generic;
using CommunEntities;
// <copyright file="ProductGestionTest.cs">Copyright �  2015</copyright>

using System;
using GunSmithBusinessLayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GunSmithBusinessLayer
{
    [TestClass]
    [PexClass(typeof(ProductGestion))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ProductGestionTest
    {
        [PexMethod]
        public void addProduct(Product product)
        {
            ProductGestion.addProduct(product);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductGestionTest.addProduct(Product)
        }
        [PexMethod]
        public IList<Product> getListProducts()
        {
            IList<Product> result = ProductGestion.getListProducts();
            return result;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductGestionTest.getListProducts()
        }
        [PexMethod]
        public void removeProduct(Product product)
        {
            ProductGestion.removeProduct(product);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductGestionTest.removeProduct(Product)
        }
        [PexMethod]
        public void updateProduct(Product product)
        {
            ProductGestion.updateProduct(product);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode ProductGestionTest.updateProduct(Product)
        }
    }
}
