using Microsoft.Pex.Framework.Generated;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommunEntities;
// <copyright file="SalesPersonGestionTest.removeSalesPerson.g.cs">Copyright �  2015</copyright>
// <auto-generated>
// Ce fichier contient des tests g�n�r�s automatiquement.
// Ne modifiez pas ce fichier manuellement.
// 
// Si le contenu de ce fichier est obsol�te, vous pouvez le supprimer.
// Par exemple, s'il n'est plus compil�.
//  </auto-generated>
using System;

namespace GunSmithBusinessLayer
{
    public partial class SalesPersonGestionTest
    {
[TestMethod]
[PexGeneratedBy(typeof(SalesPersonGestionTest))]
[PexRaisedException(typeof(NullReferenceException))]
public void removeSalesPersonThrowsNullReferenceException632()
{
    this.removeSalesPerson((SalesPerson)null);
}
    }
}
