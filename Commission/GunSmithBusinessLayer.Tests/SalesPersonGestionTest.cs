using System.Collections.Generic;
using CommunEntities;
// <copyright file="SalesPersonGestionTest.cs">Copyright �  2015</copyright>

using System;
using GunSmithBusinessLayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GunSmithBusinessLayer
{
    [TestClass]
    [PexClass(typeof(SalesPersonGestion))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class SalesPersonGestionTest
    {
        [PexMethod]
        public void updateSalesPerson(SalesPerson salesPerson)
        {
            SalesPersonGestion.updateSalesPerson(salesPerson);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonGestionTest.updateSalesPerson(SalesPerson)
        }
        [PexMethod]
        public void removeSalesPerson(SalesPerson salesPerson)
        {
            SalesPersonGestion.removeSalesPerson(salesPerson);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonGestionTest.removeSalesPerson(SalesPerson)
        }
        [PexMethod]
        public IList<SalesPerson> getListSalesPersons()
        {
            IList<SalesPerson> result = SalesPersonGestion.getListSalesPersons();
            return result;
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonGestionTest.getListSalesPersons()
        }
        [PexMethod]
        public void addSalesPerson(SalesPerson salesPerson)
        {
            SalesPersonGestion.addSalesPerson(salesPerson);
            // TODO: ajouter des assertions \u00e0 m\u00e9thode SalesPersonGestionTest.addSalesPerson(SalesPerson)
        }
    }
}
