﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunEntities;
using GunSmithEntities;

namespace GunSmithStubDataAccessLayer
{
    public class DalManager
    {
        private static DalManager mSingleton;
        public GunSmith CurrentGunSmith { get; set; }

        private DalManager()
        {
            CurrentGunSmith = null;

            GunSmiths = new List<GunSmith>();
            SalesPersons = new List<SalesPerson>();
            Products = new List<Product>();
            Cities = new List<City>();
            MonthReports = new List<MonthReport>();
            CityReports = new List<CityReport>();

            GunSmiths.Add(new GunSmith("0", "test", "test"));
            GunSmiths.Add(new GunSmith("1", "Antoine", "test"));
            GunSmiths.Add(new GunSmith("2", "Guilhem", "test"));

            SalesPersons.Add(new SalesPerson("0", "Cyrille", "test", GunSmiths[1]));
            SalesPersons.Add(new SalesPerson("1", "Robert", "test", GunSmiths[2]));
            SalesPersons.Add(new SalesPerson("2", "Daniel", "test", GunSmiths[1]));
            SalesPersons.Add(new SalesPerson("3", "Thomas", "test", GunSmiths[0]));

            Products.Add(new Product("Barillet", 20, 45, true, GunSmiths[1])) ;
            Products.Add(new Product("Canon", 50, 30, true, GunSmiths[1]));
            Products.Add(new Product("Gachette", 40, 10, true, GunSmiths[2]));
            Products.Add(new Product("Locks", 70, 30, false, GunSmiths[0]));
            Products.Add(new Product("Stocks", 80, 40, true, GunSmiths[0]));
            Products.Add(new Product("Barrels", 75, 50, true, GunSmiths[0]));


            /*Cities.Add(new City("Clermont-Ferrand")) ;
            Cities.Add(new City("Valence"));
            Cities.Add(new City("Toulon"));
            Cities.Add(new City("Lyon"));
            Cities.Add(new City("Harbin"));
            Cities.Add(new City("BDE Isima"));

            MonthReports.Add(new MonthReport(SalesPersons[1], new DateTime(10 / 2000)));
            MonthReports.Add(new MonthReport(SalesPersons[2], new DateTime(5 / 1963)));
            MonthReports.Add(new MonthReport(SalesPersons[0], new DateTime(4 / 2003)));
            MonthReports.Add(new MonthReport(SalesPersons[3], new DateTime(12 / 1987)));
            MonthReports.Add(new MonthReport(SalesPersons[0], new DateTime(7 / 1802)));

            CityReports.Add(new CityReport(MonthReports[0], 45, Cities[3])) ;
            CityReports.Add(new CityReport(MonthReports[2], 63, Cities[1]));
            CityReports.Add(new CityReport(MonthReports[4], 78, Cities[0]));
            CityReports.Add(new CityReport(MonthReports[3], 12, Cities[2]));
            CityReports.Add(new CityReport(MonthReports[1], 12, Cities[4]));*/

        }

        public static void createDalManager()
        {
            if (mSingleton == null)
                Singleton = new DalManager();
        }

        public static DalManager Singleton
        {
            get
            {
                return mSingleton;
            }
            private set
            {
                mSingleton = value;
            }
        }

        public List<GunSmith> GunSmiths { get; set; }
        public List<SalesPerson> SalesPersons { get; set; }
        public List<Product> Products { get; set; }
        public List<City> Cities { get; set; }
        public List<MonthReport> MonthReports { get; set; }
        public List<CityReport> CityReports { get; set; }
    }
}
