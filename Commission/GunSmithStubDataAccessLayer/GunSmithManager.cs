﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithStubDataAccessLayer
{
    public class GunSmithManager
    {
        public static GunSmith GunSmithFromID(String id)
        {
            GunSmith gun = null;
            foreach (GunSmith g in DalManager.Singleton.GunSmiths)
            {
                if (String.Compare(id, g.ID) == 0)
                {
                    gun = g;
                    break;
                }
            }
            return gun;
        }
    }
}
