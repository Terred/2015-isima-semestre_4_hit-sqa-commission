﻿using CommunEntities;
using GunSmithEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithStubDataAccessLayer
{
    public class MonthReportManager
    {
        public static List<MonthReport> getListMonthReports()
        {
            return DalManager.Singleton.MonthReports;
        }

        public static MonthReport getMonthReport(SalesPerson salePerson, DateTime Month)
        {
            return DalManager.Singleton.MonthReports.Where(mr => mr.Month.Month == Month.Month & mr.Seller == salePerson).FirstOrDefault();
        }

        public static MonthReport getActualMonthReport(SalesPerson salePerson)
        {
            return DalManager.Singleton.MonthReports.Where(mr => mr.EndMonth == null & mr.Seller == salePerson).FirstOrDefault();
        }

        public static void addMonthReport(MonthReport monthReport)
        {
            DalManager.Singleton.MonthReports.Add(monthReport);
        }

        public static void updateMonthReport(MonthReport monthReport)
        {
            MonthReport mr = DalManager.Singleton.MonthReports.Where(mor => mor == monthReport).First();
            mr.Seller = monthReport.Seller;
            mr.Month = monthReport.Month;
            mr.EndMonth = monthReport.Month;
            mr.TotalIncome = monthReport.TotalIncome;
            mr.Commission = monthReport.Commission;
        }
    }
}
