﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithStubDataAccessLayer
{
    public class SalesPersonManager
    {
        public static SalesPerson SalePersonFromID(String id)
        {
            return DalManager.Singleton.SalesPersons.Where(sp => sp.GunSmith == DalManager.Singleton.CurrentGunSmith & String.Compare(id, sp.ID) == 0).FirstOrDefault();
        }

        public static IList<SalesPerson> getListSalesPersons()
        {
            return DalManager.Singleton.SalesPersons.Where(sp => sp.GunSmith == DalManager.Singleton.CurrentGunSmith).ToList();
        }

        public static void addSalesPerson(SalesPerson salesPerson)
        {
            salesPerson.ID = (int.Parse(DalManager.Singleton.SalesPersons.OrderByDescending(sp => sp.ID).First().ID) + 1).ToString();
            DalManager.Singleton.SalesPersons.Add(salesPerson);
        }

        public static void updateSalesPerson(SalesPerson salesPerson)
        {
            SalesPerson sp = DalManager.Singleton.SalesPersons.Where(s => s == salesPerson).First();
            sp.Name = salesPerson.Name;
            sp.ID = salesPerson.ID;
            sp.EncryptedPassword = salesPerson.EncryptedPassword;
            sp.GunSmith = sp.GunSmith;
        }

        public static void removeSalesPerson(SalesPerson salesPerson)
        {
            DalManager.Singleton.SalesPersons.Remove(salesPerson);
        }
    }
}
