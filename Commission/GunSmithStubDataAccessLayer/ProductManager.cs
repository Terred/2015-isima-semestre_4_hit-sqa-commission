﻿using CommunEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithStubDataAccessLayer
{
    public class ProductManager
    {
        public static IList<Product> getListProducts()
        {
            return DalManager.Singleton.Products.Where(prod => prod.GunSmith == DalManager.Singleton.CurrentGunSmith).ToList();
        }

        public static void addProduct(Product product)
        {
            DalManager.Singleton.Products.Add(product);
        }

        public static void updateProduct(Product product)
        {
            Product pr = DalManager.Singleton.Products.Where(p => p == product).First();
            pr.Name = product.Name;
            pr.Price = product.Price;
            pr.SaleMandatory = product.SaleMandatory;
            pr.Amount = product.Amount;
            pr.GunSmith = pr.GunSmith;
        }

        public static void removeProduct(Product product)
        {
            DalManager.Singleton.Products.Remove(product);
        }
    }
}
