﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GunSmithStubDataAccessLayer
{
    public class CityReportManager
    {
        public static void addCityReport(GunSmithEntities.CityReport cityReport)
        {
            DalManager.Singleton.CityReports.Add(cityReport);
        }
    }
}
